erlang-p1-acme (1.0.25-1) unstable; urgency=medium

  * New upstream version 1.0.25
  * Updated Standards-Version: 4.7.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Removed debian/patches/ because it is no longer needed

 -- Philipp Huebner <debalance@debian.org>  Sun, 09 Feb 2025 11:26:34 +0100

erlang-p1-acme (1.0.23-1) unstable; urgency=medium

  * New upstream version 1.0.23
  * Updated Erlang dependencies
  * Updated debian/patches/rebar.config.diff

 -- Philipp Huebner <debalance@debian.org>  Thu, 03 Oct 2024 21:35:58 +0200

erlang-p1-acme (1.0.22-2) unstable; urgency=medium

  * Updated lintian overrides
  * Dropped debian/erlang-p1-acme.install
  * Renamed debian/erlang-p1-acme.lintian-overrides
    -> debian/lintian-overrides
  * Updated debian/patches/rebar.config.diff

 -- Philipp Huebner <debalance@debian.org>  Tue, 26 Dec 2023 22:55:47 +0100

erlang-p1-acme (1.0.22-1) unstable; urgency=medium

  * New upstream version 1.0.22
  * Refreshed patches
  * Updated Erlang dependencies
  * Updated Standards-Version: 4.6.2 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 08 Feb 2023 21:29:16 +0100

erlang-p1-acme (1.0.20-1) unstable; urgency=medium

  * New upstream version 1.0.20
  * Updated Erlang dependencies
  * Refreshed debian/patches/rebar.config.diff

 -- Philipp Huebner <debalance@debian.org>  Wed, 02 Nov 2022 15:24:12 +0100

erlang-p1-acme (1.0.19-1) unstable; urgency=medium

  * New upstream version 1.0.19
  * Updated Standards-Version: 4.6.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Thu, 02 Jun 2022 13:01:34 +0200

erlang-p1-acme (1.0.16-1) unstable; urgency=medium

  [ Philipp Huebner ]
  * New upstream version 1.0.16
  * Updated Erlang dependencies
  * Updated debian/watch

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Philipp Huebner <debalance@debian.org>  Mon, 20 Dec 2021 19:49:43 +0100

erlang-p1-acme (1.0.13-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch

  [ Philipp Huebner ]
  * New upstream version 1.0.13
  * Updated Standards-Version: 4.6.0 (no changes needed)
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 22:10:13 +0200

erlang-p1-acme (1.0.11-2) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 19:41:27 +0100

erlang-p1-acme (1.0.11-1) unstable; urgency=medium

  * New upstream version 1.0.11
  * Added 'Multi-Arch: same' in debian/control
  * Updated Erlang dependencies
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 19:14:52 +0100

erlang-p1-acme (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-acme.install

 -- Philipp Huebner <debalance@debian.org>  Sat, 26 Dec 2020 00:59:17 +0100

erlang-p1-acme (1.0.8-1) unstable; urgency=medium

  * New upstream version 1.0.8
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 18:14:41 +0200

erlang-p1-acme (1.0.7-1) unstable; urgency=medium

  * New upstream version 1.0.7
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 22:32:30 +0200

erlang-p1-acme (1.0.5-1) unstable; urgency=medium

  * New upstream version 1.0.5
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 18 Apr 2020 09:50:01 +0200

erlang-p1-acme (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Wed, 18 Mar 2020 13:57:36 +0100

erlang-p1-acme (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Feb 2020 21:01:54 +0100

erlang-p1-acme (1.0.2-2) unstable; urgency=medium

  * Source-only upload.

 -- Philipp Huebner <debalance@debian.org>  Thu, 14 Nov 2019 19:18:21 +0100

erlang-p1-acme (1.0.2-1) unstable; urgency=medium

  * Initial release (Closes: #944532)

 -- Philipp Huebner <debalance@debian.org>  Mon, 11 Nov 2019 16:15:34 +0100
